package com.example.muscleday;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.muscleday.persistencia.DbUsuarios;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;


public class IniciarSesionActivity extends AppCompatActivity {

    // Dialog
   // private Dialog dialog;
    //private Button ShowDialog;
    EditText username, password;
    Button iniciarSesionBtn;
    DbUsuarios DB;
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iniciar_sesion);

        // Dialog
       // ShowDialog = findViewById(R.id.iniciarSesionBtn);



        getSupportActionBar().hide(); // Eliminamos la barrar superior

        username = findViewById(R.id.correo);
        password = findViewById(R.id.contrasena);

        MaterialButton iniciarSesionBtn = (MaterialButton) findViewById(R.id.iniciarSesionBtn);

        DB = new DbUsuarios(this);

        iniciarSesionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = username.getText().toString();
                String pass = password.getText().toString();

                if (TextUtils.isEmpty(user) || TextUtils.isEmpty(pass))
                    Toast.makeText(IniciarSesionActivity.this, "Todos los espacios son requeridos", Toast.LENGTH_SHORT).show();
                else {
                    DB.checkcontrasena(user, pass,new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            Toast.makeText(IniciarSesionActivity.this, "Verificando usuario", Toast.LENGTH_SHORT).show();
                            if(task.isSuccessful()){ // verificamos si se hizo correctamente la peticion al server
                                for (DocumentSnapshot doc : task.getResult()){
                                    if(doc.exists()){
                                        // Si el usuario ya existe.
                                        String contraenfirebase = doc.getString("contrasena");
                                        if (contraenfirebase.equals(pass)) {
                                            Toast.makeText(IniciarSesionActivity.this, "Login correcto", Toast.LENGTH_SHORT).show();
                                            Dialogo d = new Dialogo(IniciarSesionActivity.this,"¿Desea ingresar?","Bienvenid@ a MuscleDay", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    Intent intent = new Intent(getApplicationContext(), CardsItems.class);
                                                    startActivity(intent);
                                                }
                                            });
                                        } else {
                                            Toast.makeText(IniciarSesionActivity.this, "Login incorrecto", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    else {
                                        //El usuario no existe.
                                        // Aquí lo insertaríamos.
                                    }
                                }
                            } else {
                                // Hubo un error en la conexión.
                            }
                        }
                    });
                }
            }

        });


        //   btnRegis.setOnClickListener(new View.OnClickListener() {

        // Dialogo d = new Dialogo(IniciarSesionActivity.this,"¿Desea ingresar?","Bienvenid@ a MuscleDay", new View.OnClickListener() {
      //      @Override
      //      public void onClick(View view) {
                // Cambiamos de layout/Vista
      //          Intent intent = new Intent(getApplicationContext(), Registro.class);
      //          startActivity(intent);
       //     }
      //  });
    }
}




//     Dialog dialog; // Nuevo

        // conectamos la clase .java
    //    dialog = new Dialog(IniciarSesionActivity.this); // Indicamos donde se mostrará el el Dialog
        // conectamos al .xml
    //    dialog.setContentView(R.layout.custom_dialog); // Conexión al archivo custom_dialog.xml
    //    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
    //        dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.background2));

     //   }

     //   dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.background2));// Asiganamos el Fondo del Dialog (Background)
     //   dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
     //   dialog.setCancelable(false);

        // Declaramos los botones
     //   Button Okey = dialog.findViewById(R.id.btn_okay);
     //   Button Cancel = dialog.findViewById(R.id.btn_cancel);

     //   Okey.setOnClickListener(new View.OnClickListener() {
     //       @Override
      //      public void onClick(View v) {

    //            Toast.makeText(IniciarSesionActivity.this, "Okay", Toast.LENGTH_SHORT).show();
      //          dialog.dismiss();
   //         }
   //     });

     //   Cancel.setOnClickListener(new View.OnClickListener() {
     //       @Override
     //       public void onClick(View v) {

     //           Toast.makeText(IniciarSesionActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
     //           dialog.dismiss();
      //      }
     //   });


    //    ShowDialog.setOnClickListener(new View.OnClickListener() {
     //       @Override
     //       public void onClick(View v) {

     //           dialog.show();
     //       }
     //   });

        // -----------FIN CODIGO-----------------

//        loginbtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (username.getText().toString().equals("admin") && password.getText().toString().equals("admin")){
//                    Toast.makeText(MainActivity.this, "Usuario correcto", Toast.LENGTH_SHORT).show();
//                }else
//                    Toast.makeText(MainActivity.this,"Usuario no registrado", Toast.LENGTH_SHORT).show();
//
//            }
//        });
 //   }
 //   public void regis(View m){
        // Instanciamos nuestro objeto "cambiar"
 //       Intent cambiar = new Intent(IniciarSesionActivity.this, ejercicios_activity.class);
 //       startActivity(cambiar);


 //   }


//}

package com.example.muscleday.persistencia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.example.muscleday.AdapterArticulos;
import com.example.muscleday.ListArticulos;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DbProductos extends DbHelper {

    Context context; // Variable global
    FirebaseFirestore firebaseFirestore;
    FirebaseFirestore dbf = firebaseFirestore.getInstance();
    AdapterArticulos adapterArticulos;
    RecyclerView recyclerView;

    // Constructor
    public DbProductos(@Nullable Context context) {
        super(context);
        this.context = context;
    }

    public void agregarArticulo(String nombrep, String musculo, String descripcion) {
        ContentValues cv = new ContentValues(); // Instancia del objeto ContentValues
        cv.put("nombrep", nombrep);
        cv.put("musculo", musculo);
        cv.put("descripcion", descripcion);
        this.getWritableDatabase().insert("ejercicios", null, cv);
    }

    public void eliminarArticulo(String codigop) {
        this.getWritableDatabase().delete("ejercicios", "codigop = ?", new String[]{codigop.trim()});
    }

    public void actualizarArticulo(String codigop, String musculo, String descripcion) {
        ContentValues cv = new ContentValues(); // Instancia
        cv.put("musculo", musculo);
        cv.put("descripcion", descripcion);
        // trim = Es un método que se encarga de eliminar caracteres blancos iniciales y finales de una cadena de texto (String)
        this.getWritableDatabase().update("ejercicios", cv, "codigop = ?", new String[]{codigop.trim()});
    }

    public List<ListArticulos> consultarArticulos() {
        List<ListArticulos> listArticulos = new ArrayList<ListArticulos>(); // Instancia de un objeto tipo lista

        Cursor result = this.getWritableDatabase().query("ejercicios", new String[]{"codigop", "nombrep", "musculo", "descripcion"}, null, null, null, null, null);
        while (result.moveToNext()) {
            ListArticulos nuevoArticulo = new ListArticulos(
                    result.getInt((int) result.getColumnIndex("codigop")),
                    result.getString((int) result.getColumnIndex("nombrep")),
                    result.getString((int) result.getColumnIndex("musculo")),
                    result.getString((int) result.getColumnIndex("descripcion"))
            );
            listArticulos.add(nuevoArticulo);
        }

        return listArticulos;
    }
}






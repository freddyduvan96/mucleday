package com.example.muscleday;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.muscleday.persistencia.DbUsuarios;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;


public class RegistrarActivity extends AppCompatActivity {
    EditText username1, password1, repassword, correo;
    Button iniciarSesionBtn, volverSining; // Variable Global
    DbUsuarios DB;

    // Ciclo de vida de android = se ejecuta al iniciar nuestra activity onCreate, onStart, onResume.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

        username1 = findViewById(R.id.nombreRegistrar);
        password1 = findViewById(R.id.contrasena);
        repassword = findViewById(R.id.contrasena2);
        correo = findViewById(R.id.correo);
        iniciarSesionBtn = findViewById(R.id.iniciarSesionBtn);
        volverSining = findViewById(R.id.volverSining);
        DB = new DbUsuarios(this);

        iniciarSesionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = username1.getText().toString(); //
                String pass = password1.getText().toString();
                String repass = repassword.getText().toString();
                String email = correo.getText().toString();

                if (TextUtils.isEmpty(user) || TextUtils.isEmpty(pass) || TextUtils.isEmpty(repass) || TextUtils.isEmpty(email))
                    Toast.makeText(RegistrarActivity.this, "Se requiere llenar los campos", Toast.LENGTH_SHORT).show();
                else{
                    if (pass.equals(repass)) {
                        DB.checknomusuario(user,new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if(task.isSuccessful()){ // verificamos si se hizo correctamente la peticion al server
                                    int encontrado = 0;
                                    for (DocumentSnapshot doc : task.getResult()){
                                        if(doc.exists()) {
                                            // Si el usuario ya existe.
                                            Toast.makeText(RegistrarActivity.this, "El usuario ya existe", Toast.LENGTH_SHORT).show();
                                            encontrado = encontrado + 1;
                                        }
                                    }

                                    if(encontrado == 0)  {
                                        //El usuario no existe.
                                        // Aquí lo insertaríamos.
                                        DB.insertarUsuario(user, pass, email,new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void unused) {
                                                //el usuario fue insertado
                                                Toast.makeText(RegistrarActivity.this, "Registrado correctamente", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                startActivity(intent);
                                            }
                                        },new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Toast.makeText(RegistrarActivity.this, "Registro fallido", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                } else {
                                    // Hubo un error en la conexión.
                                }
                            }
                        });
                    } else {
                        Toast.makeText(RegistrarActivity.this, "La contraseña no coincide", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        });

        volverSining.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new  Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);

            }
        });

    }
}


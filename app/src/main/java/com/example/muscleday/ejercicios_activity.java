package com.example.muscleday;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class ejercicios_activity extends AppCompatActivity {
    List<ListElement> elements; // NEW

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicios);

        init(); // NEW

    }
    public void init(){
        elements = new ArrayList<ListElement>(); // Instancia del objeto elements
        elements.add(new ListElement("#775447", "Pecho", "Lunes", "Activo")); // Tarjeta 1
        elements.add(new ListElement("#607d8b", "Espalda", "Martes", "Activo")); // Tarjeta 2
        elements.add(new ListElement("#03a9f4", "Pierna", "Miercoles", "Activo")); // Tarjeta 3
        elements.add(new ListElement("#009688", "Brazo", "Jueves", "Activo")); // Tarjeta 4
        elements.add(new ListElement("#607d8b", "Hombro", "viernes", "Activo")); // Tarjeta 5


        // Declaramos el ListAdapter y recibe una lista y el context = de donde viene
        ListAdapter listAdapter = new ListAdapter(elements, this);
        // Declaramos el RecyclerView
        RecyclerView recyclerView = findViewById(R.id.listRecyclerView);
        // Movemos unos parametros en verdadero
        recyclerView.setHasFixedSize(true);
        // Listado lineal setLayoutManager
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        recyclerView.setAdapter(listAdapter);

    }
}

package com.example.muscleday;

public class ListElement {
    public String color;
    public String name;
    public String day;
    public String status;

    // Constructor
    public ListElement(String color, String name, String day, String status) {
        this.color = color;
        this.name = name;
        this.day = day;
        this.status = status;
    }
    // GET = obtener el dato
    // SET = Modificar el dato

    // Métodos GET y SET
    public String getColor() {

        return color;
    }

    public void setColor(String color) {

        this.color = color;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;

    }

    public String getDay() {

        return day;
    }

    public void setDay(String day) {

        this.day= day;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }



}
